declare module 'react-game-kit';
declare module 'ascii-table';
declare module 'react-typist';

declare module '*.wav' {
  const path: string;
  export default path;
}

declare var module: NodeModule & {
  hot: {
    accept: any;
    dispose: any;
  };
};
