import * as React from 'react';
import { hot } from 'react-hot-loader';

import Game from './Game';

type State = {};

class App extends React.Component<{}, State> {
  state = {};

  render() {
    return <Game />;
  }
}

export default hot(module)(App);
