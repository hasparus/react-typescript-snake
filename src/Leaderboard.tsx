import AsciiTable from 'ascii-table';
import React from 'react';
import Typist from 'react-typist';
import styled from 'styled-components';

import Overlay from './layout/Overlay';
import Score, { getScores } from './scores';

const Pre = styled.pre`
  background: rgba(0, 0, 0, 0.1);
  padding: 1em;
  font-family: inherit;
  margin: 0;
`;

type LeaderboardState = {
  scores: Score[];
  failedToFetch: boolean;
  renderedAtLeastOnce: boolean;
};

type LeaderboardProps = {
  visible: boolean;
};
class Leaderboard extends React.PureComponent<
  LeaderboardProps,
  LeaderboardState
> {
  state: LeaderboardState = {
    scores: [],
    failedToFetch: false,
    renderedAtLeastOnce: false,
  };

  private fetchScores = () => {
    getScores()
      .then(response => {
        if (response.ok) {
          return response.json().then(scores => {
            this.setState({ scores });
          });
        }
        throw new Error('Bad response.');
      })
      .catch(reason => {
        // tslint:disable-next-line no-console
        console.error(reason);
        this.setState({ failedToFetch: true });
        // error state in UI?
      });
  };

  private handleTypingDone = () => {
    this.setState({ renderedAtLeastOnce: true });
  };

  componentDidMount() {
    this.fetchScores();
  }

  componentDidUpdate(prevProps: LeaderboardProps) {
    if (this.props.visible && !prevProps.visible) {
      this.fetchScores();
    }
  }

  render() {
    const { visible } = this.props;
    if (!visible) {
      return null;
    }
    const { failedToFetch, scores, renderedAtLeastOnce } = this.state;

    const table = new AsciiTable('TOP 10');
    if (failedToFetch) {
      table.addRow("Couldn't get scores ;c");
    } else {
      scores.forEach(score =>
        table.addRow(score.player, score.value)
      );
    }

    return (
      <Overlay>
        {renderedAtLeastOnce ? (
          <Pre>{table.toString()}</Pre>
        ) : (
          <Typist
            avgTypingDelay={8}
            cursor={{
              show: false,
            }}
            onTypingDone={this.handleTypingDone}
          >
            <Pre>{table.toString()}</Pre>
          </Typist>
        )}
      </Overlay>
    );
  }
}

export default Leaderboard;
