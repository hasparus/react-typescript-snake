export const enum Color {
  Yellow = '#ffba08',
  Blue = '#3f88c5',
  Pink = 'pink',
  White = '#ffffff',
}
