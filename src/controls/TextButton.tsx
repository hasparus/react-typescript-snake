import { css } from 'styled-components';

import Button from './Button';

const TextButton = Button.extend`
  user-select: none;
  ${({ disabled }: { disabled?: boolean }) =>
    disabled
      ? css`
          background: none;
          cursor: default;
          color: rgba(255, 255, 255, 0.25);
        `
      : css`
          &:hover,
          &:active {
            background: rgba(0, 0, 0, 0.25);
          }
        `};
`;

export default TextButton;
