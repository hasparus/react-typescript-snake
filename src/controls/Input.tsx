import styled from 'styled-components';

const Input = styled.input`
  border: none;
  margin: 0;
  padding: 0;
  width: auto;
  overflow: visible;

  background: transparent;

  outline: none;
  text-align: inherit;
  color: inherit;
  font: inherit;

  line-height: normal;

  -webkit-font-smoothing: inherit;
  -moz-osx-font-smoothing: inherit;
  -webkit-appearance: none;

  &:hover,
  &:active {
    background: rgba(0, 0, 0, 0.25);
  }
`;

export default Input;
