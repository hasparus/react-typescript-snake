import { debounce } from 'lodash/fp';

const PLAYER_NAME_STORAGE_KEY = 'playerName';
export const DEFAULT_PLAYER_NAME = 'UNKNOWN PLAYER';

export const getPersistedPlayerName = () =>
  localStorage.getItem(PLAYER_NAME_STORAGE_KEY) || '';

export const persistPlayerName = debounce(500, (value: string) =>
  localStorage.setItem(PLAYER_NAME_STORAGE_KEY, value)
);
