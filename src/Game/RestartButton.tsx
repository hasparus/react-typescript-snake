import React from 'react';

import { TextButton } from '../controls';

export type RestartButtonProps = {
  handleRestart: () => void;
};
const RestartButton = ({ handleRestart }: RestartButtonProps) => (
  <TextButton onClick={handleRestart}>Restart (R)</TextButton>
);

export default RestartButton;
