import React from 'react';
import styled from 'styled-components';

const Container = styled.footer`
  display: flex;
  flex-direction: row;
  h1 {
    flex: 1;
    text-align: center;
    color: white;
    font-size: 2.5rem;
    margin: 1rem;
    user-select: none;
  }
`;

const Footer = () => (
  <Container>
    <h1>Snake</h1>
  </Container>
);

export default Footer;
