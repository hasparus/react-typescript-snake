import PropTypes from 'prop-types';
import React from 'react';
import { Loop } from 'react-game-kit';
import styled, { css } from 'styled-components';

import { Color } from '../color';
import {
  DEFAULT_PLAYER_NAME,
  getPersistedPlayerName,
  persistPlayerName,
} from '../player_name';
import { sendScore } from '../scores';
import Leaderboard from '../Leaderboard';

import {
  BASE_SPEED_MODIFIER,
  movementVectors,
  Vector2,
} from './core';
import './sounds';
import * as SoundContext from './sounds/Context';
import Candy, {
  Candy as CandyState,
  makeInitialCandies,
  updateCandies,
} from './Candy';
import Footer from './Footer';
import Header from './Header';
import * as Overlay from './Overlay';
import Snake, {
  Snake as SnakeState,
  turnSnake,
  updateSnake,
} from './Snake';
import SvgCanvas from './SvgCanvas';

const Article = styled.article`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  outline: none;
  color: white;

  *::selection {
    background: ${Color.Blue};
  }

  transition: filter ease-out 300ms;
  ${({ invert }: { invert: boolean }) =>
    invert
      ? css`
          filter: grayscale(0.6) invert(1);
        `
      : ''};
`;

const Background = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  transition: filter ease-out 300ms;
  z-index: -1;

  ${({ desaturate }: { desaturate: boolean }) =>
    desaturate
      ? css`
          filter: invert(0.1) grayscale(0.3);
        `
      : ''} background: radial-gradient(circle at 0 50%, #41295a, #2f0743);
`;

type GameContainerProps = {
  gameOver: boolean;
  gamePaused: boolean;
  tabIndex: number;
  onKeyDown: React.KeyboardEventHandler<HTMLElement>;
  children: React.ReactNode;
};

const GameContainer = ({
  children,
  gamePaused,
  gameOver,
  ...rest
}: GameContainerProps) => (
  <Article {...rest} invert={gameOver}>
    <Background desaturate={gamePaused && !gameOver} />
    {children}
  </Article>
);

type Props = {
  toggleSound: SoundContext.SoundContextType['toggleSound'];
};

type State = {
  playerName: string;
  gamePaused: boolean;
  leadeboardVisible: boolean;
  candies: CandyState[];
  score: number;
  snake: SnakeState;
};

const makeInitialState = (persistedState: Partial<State> = {}) => ({
  playerName: getPersistedPlayerName(),
  gamePaused: false,
  leadeboardVisible: false,
  candies: makeInitialCandies(4),
  score: 0,
  snake: {
    headPosition: { x: 0, y: 0 },
    velocity: { x: BASE_SPEED_MODIFIER, y: 0 },
    tail: [],
    isDead: false,
  },
  ...persistedState,
});

export class Game extends React.Component<Props, State> {
  static contextTypes = {
    loop: PropTypes.object,
  };

  alreadyTurned = false;
  ticks = 0;
  state = makeInitialState();

  private onUpdate = () => {
    this.ticks += 1;
    if (this.ticks % 8 !== 1) return;
    this.alreadyTurned = false;

    this.setState(oldState => {
      const { snake: oldSnake, candies: oldCandies } = oldState;

      const {
        candies,
        candiesEaten,
        destroyedCandies: newSnakeSegments,
      } = updateCandies(
        oldCandies,
        oldSnake.headPosition,
        oldSnake.tail
      );

      const snake = updateSnake(oldSnake, newSnakeSegments);

      if (snake.isDead) {
        sendScore({
          player: this.state.playerName || DEFAULT_PLAYER_NAME,
          value: this.state.score,
          created_at: Date.now(),
        });
        this.context.loop.unsubscribe(this.onUpdate);
      }

      return {
        candies,
        snake,
        leadeboardVisible:
          oldState.leadeboardVisible && !snake.isDead,
        gamePaused: snake.isDead,
        score: oldState.score + candiesEaten,
      };
    });
  };

  private controlSnake = (newVelocity: Vector2) => {
    if (!this.alreadyTurned) {
      const { velocity: oldVelocity } = this.state.snake;
      if (
        (oldVelocity.x && newVelocity.y) ||
        (oldVelocity.y && newVelocity.x)
      ) {
        this.alreadyTurned = true;
        this.setState(({ snake }) => ({
          snake: turnSnake(snake, newVelocity),
        }));
      } else {
        // you can't turn in place but maybe some booster or slowdown here is possible
      }
    }
  };

  private handleKeyDown = (
    event: React.KeyboardEvent<HTMLElement>
  ) => {
    const key =
      event.key.length === 1 ? event.key.toLowerCase() : event.key;

    let newVelocity: Vector2 | undefined;
    switch (key) {
      case 'd':
      case 'ArrowRight':
        newVelocity = movementVectors.Right;
        break;
      case 's':
      case 'ArrowDown':
        newVelocity = movementVectors.Down;
        break;
      case 'a':
      case 'ArrowLeft':
        newVelocity = movementVectors.Left;
        break;
      case 'w':
      case 'ArrowUp':
        newVelocity = movementVectors.Up;
        break;
      case 'l':
        this.toggleLeaderboard();
        break;
      case 'm':
        this.props.toggleSound();
        break;
      case 'p':
        if (this.state.gamePaused) {
          this.runGame();
        } else {
          this.pauseGame();
        }
        break;
      case 'r':
        this.handleRestart();

      default:
        return;
    }
    if (newVelocity) {
      this.controlSnake(newVelocity);
    }
  };

  private handlePlayerNameChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const playerName = event.target.value;
    this.setState({
      playerName,
    });
    persistPlayerName(playerName);
  };

  private handleRestart = () => {
    this.pauseGame();
    this.setState(
      ({ playerName }) => makeInitialState({ playerName }),
      this.runGame
    );
  };

  private runGame = () => {
    this.context.loop.subscribe(this.onUpdate);
    this.setState({ gamePaused: false });
  };

  private pauseGame = () => {
    this.context.loop.unsubscribe(this.onUpdate);
    this.setState({ gamePaused: true });
  };

  private togglePause = () => {
    if (this.state.gamePaused) {
      this.runGame();
    } else {
      this.pauseGame();
    }
  };

  private toggleLeaderboard = () => {
    this.setState(({ leadeboardVisible }) => ({
      leadeboardVisible: !leadeboardVisible,
    }));
  };

  componentDidMount() {
    this.runGame();
  }

  componentWillUnmount() {
    this.pauseGame();
  }

  render() {
    const {
      score,
      snake,
      candies,
      playerName,
      gamePaused,
      leadeboardVisible,
    } = this.state;

    return (
      <GameContainer
        gameOver={snake.isDead}
        gamePaused={gamePaused}
        tabIndex={0}
        onKeyDown={this.handleKeyDown}
      >
        <Header
          score={score}
          gamePaused={gamePaused}
          toggleLeaderboard={this.toggleLeaderboard}
          togglePause={snake.isDead ? null : this.togglePause}
          handleRestart={this.handleRestart}
          playerName={playerName}
          handlePlayerNameChange={this.handlePlayerNameChange}
          defaultPlayerName={DEFAULT_PLAYER_NAME}
        />
        <SvgCanvas>
          <Snake {...snake} />
          {candies.map(candy => (
            <Candy {...candy} key={`${candy.id}c`} />
          ))}
        </SvgCanvas>
        <Footer />
        <Leaderboard visible={leadeboardVisible} />
        {gamePaused &&
          !leadeboardVisible &&
          (snake.isDead ? (
            <Overlay.GameOver handleRestart={this.handleRestart} />
          ) : (
            <Overlay.Pause />
          ))}
      </GameContainer>
    );
  }
}

const WrappedGame = () => (
  <SoundContext.Provider>
    <Loop>
      <SoundContext.Consumer>
        {({ toggleSound }) => <Game toggleSound={toggleSound} />}
      </SoundContext.Consumer>
    </Loop>
  </SoundContext.Provider>
);

export default WrappedGame;
