import React from 'react';

import { Color } from '../../color';

import { Snake as SnakeT } from './snake';
import SnakePart from './SnakePart';

const Snake = ({ headPosition, tail }: SnakeT) => (
  <>
    <SnakePart {...headPosition} color={Color.Yellow} />
    {tail.map((point, index) => (
      <SnakePart key={`${index}sp`} {...point} />
    ))}
  </>
);

export default Snake;
export * from './snake';
