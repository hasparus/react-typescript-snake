import { collides, move, Vector2 } from '../core';
import { playSound, Sound } from '../sounds';

export type TurningPoint = {
  position: Vector2;
  arrow: Vector2;
};

export const createTurningPoint = (
  headPosition: Vector2,
  velocity: Vector2
) => {
  const res = {
    position: headPosition,
    arrow: velocity,
  };
  return res;
};

export type Snake = {
  headPosition: Vector2;
  velocity: Vector2;
  tail: Vector2[];
  isDead: boolean;
};

export function updateSnake(
  oldSnake: Snake,
  newSnakeSegments: Array<{ id: any }>
): Snake {
  const {
    velocity,
    tail: oldTail,
    headPosition: oldHeadPosition,
  } = oldSnake;

  const headPosition = move(oldHeadPosition, velocity);

  const tail = oldTail.concat([oldHeadPosition]);
  if (!newSnakeSegments.length) {
    tail.shift();
  }

  if (tail.some(collides(headPosition))) {
    playSound(Sound.Explosion);
    return {
      headPosition: oldHeadPosition,
      velocity: { x: 0, y: 0 },
      tail: oldTail,
      isDead: true,
    };
  }

  return {
    headPosition,
    velocity,
    tail,
    isDead: oldSnake.isDead,
  };
}

export function turnSnake(snake: Snake, newVelocity: Vector2): Snake {
  return {
    ...snake,
    headPosition: snake.headPosition,
    velocity: newVelocity,
  };
}
