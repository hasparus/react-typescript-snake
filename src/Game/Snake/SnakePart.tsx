import React from 'react';
import styled from 'styled-components';

import { Color } from '../../color';
import { CELL_SIZE, ENTITY_SIZE } from '../core';

type SnakeCircleProps = { color?: Color };
const SnakeCircle = styled.circle`
  stroke: ${({ color = Color.White }: SnakeCircleProps) => color};
  stroke-width: ${ENTITY_SIZE / 10};
  fill: transparent;
  transform: translate3d(
    ${ENTITY_SIZE / 2}px,
    ${ENTITY_SIZE / 2}px,
    0
  );
`;

type SnakePartProps = {
  x: number;
  y: number;
} & SnakeCircleProps;

const SnakePart = ({ x, y, ...rest }: SnakePartProps) => (
  <SnakeCircle
    cx={x * CELL_SIZE}
    cy={y * CELL_SIZE}
    r={ENTITY_SIZE / 2 - ENTITY_SIZE / 10}
    {...rest}
  />
);

export default SnakePart;
