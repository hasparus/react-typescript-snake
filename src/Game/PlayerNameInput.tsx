import React from 'react';

import { Input } from '../controls';

export type PlayerNameInputProps = {
  playerName: string;
  handlePlayerNameChange: React.ChangeEventHandler<HTMLInputElement>;
  defaultPlayerName: string;
};

class PlayerNameInput extends React.PureComponent<
  PlayerNameInputProps
> {
  private handleKeyDown = (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    event.stopPropagation();
  };

  render() {
    const {
      playerName,
      handlePlayerNameChange,
      defaultPlayerName,
    } = this.props;

    return (
      <Input
        value={playerName}
        onChange={handlePlayerNameChange}
        onKeyDown={this.handleKeyDown}
        placeholder={defaultPlayerName}
        style={{
          width: '6em',
        }}
      />
    );
  }
}

export default PlayerNameInput;
