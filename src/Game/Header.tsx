import React from 'react';
import styled from 'styled-components';

import LeaderboardButton, {
  LeaderboardButtonProps,
} from './LeaderboardButton';
import PauseButton, { PauseButtonProps } from './PauseButton';
import PlayerNameInput, {
  PlayerNameInputProps,
} from './PlayerNameInput';
import RestartButton, { RestartButtonProps } from './RestartButton';
import Score, { ScoreProps } from './Score';
import SoundControls from './SoundControls';

const Container = styled.header`
  align-content: space-between;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  height: 51px;
  max-width: 500px;
  padding: 1rem;
  z-index: 1;
`;

const WrappingContainer = ({
  children,
}: {
  children: React.ReactNode[];
}) => (
  <Container>
    {children.map((child, index) => <div key={index}>{child}</div>)}
  </Container>
);

type HeaderProps = LeaderboardButtonProps &
  PauseButtonProps &
  PlayerNameInputProps &
  RestartButtonProps &
  ScoreProps;
const Header = ({
  defaultPlayerName,
  gamePaused,
  togglePause,
  handlePlayerNameChange,
  handleRestart,
  toggleLeaderboard,
  playerName,
  score,
}: HeaderProps) => (
  <WrappingContainer>
    <Score score={score} />
    <PlayerNameInput
      playerName={playerName}
      handlePlayerNameChange={handlePlayerNameChange}
      defaultPlayerName={defaultPlayerName}
    />
    <SoundControls />
    <RestartButton handleRestart={handleRestart} />
    <PauseButton gamePaused={gamePaused} togglePause={togglePause} />
    <LeaderboardButton toggleLeaderboard={toggleLeaderboard} />
  </WrappingContainer>
);

export default Header;
