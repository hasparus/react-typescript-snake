import React, { ReactNode } from 'react';
import styled from 'styled-components';

import { BOARD_UNITS } from './core';

const SVG_MARGIN = '10px';
const Svg = styled.svg`
  display: flex;
  max-height: calc(100% - 150px);
  margin: ${SVG_MARGIN};
  shape-rendering: geometricPrecision;
  & > rect {
    fill: rgba(0, 0, 0, 0.1);
  }
`;

const SvgCanvas = ({
  children,
  ...rest
}: {
  children: ReactNode;
}) => (
  <Svg
    {...rest}
    xmlns="http://www.w3.org/2000/svg"
    version="1.1"
    preserveAspectRatio="xMidYMin meet"
    viewBox={`0 0 ${BOARD_UNITS} ${BOARD_UNITS}`}
  >
    <rect width="100%" height="100%" />
    {children}
  </Svg>
);

export default SvgCanvas;
