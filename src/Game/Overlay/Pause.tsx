import React from 'react';

import Overlay from '../../layout/Overlay';

type PauseOverlayProps = {};
const PauseOverlay = ({  }: PauseOverlayProps) => (
  <Overlay>
    <h1>Paused</h1>
  </Overlay>
);

export default PauseOverlay;
