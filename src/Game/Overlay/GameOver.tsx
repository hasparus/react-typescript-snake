import React from 'react';

import { TextButton } from '../../controls';

import Overlay from '../../layout/Overlay';

type GameOverOverlayProps = {
  handleRestart: () => void;
};
const GameOverOverlay = ({ handleRestart }: GameOverOverlayProps) => (
  <Overlay>
    <h1>Game Over</h1>
    <TextButton onClick={handleRestart}>Wanna play again?</TextButton>
  </Overlay>
);

export default GameOverOverlay;
