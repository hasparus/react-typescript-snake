import { Vector as Vector2 } from 'matter-js';
import { map } from 'ramda';

export const BOARD_UNITS = 64;
export const CELL_SIZE = 1;
export const ENTITY_CELLSIZE = 1;
export const ENTITY_SIZE = CELL_SIZE * ENTITY_CELLSIZE;
export const CELLS = BOARD_UNITS / CELL_SIZE;

// math:
const contain = (x: number) => (CELLS + x) % CELLS;

export const randomPosition = () => ({
  x: Math.floor(Math.random() * CELLS),
  y: Math.floor(Math.random() * CELLS),
});

export const move = (position: Vector2, velocity: Vector2): Vector2 =>
  map(contain, Vector2.add(position, velocity));

export const directionVectors = {
  Up: { x: 0, y: -1 },
  Right: { x: 1, y: 0 },
  Down: { x: 0, y: 1 },
  Left: { x: -1, y: 0 },
};

export const BASE_SPEED_MODIFIER = 1;
const multiplyByBaseSpeed = (x: number) => x * BASE_SPEED_MODIFIER;
export const movementVectors = (map(
  map(multiplyByBaseSpeed),
  directionVectors
) as any) as typeof directionVectors;

export const collides = (u: Vector2) => (v: Vector2) =>
  u.x === v.x && u.y === v.y;

export { Vector2 };
