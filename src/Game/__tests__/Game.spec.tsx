import { shallow } from 'enzyme';
import * as React from 'react';

jest.mock('../sounds/index.ts');
jest.mock('../../player_name.ts');

// tslint:disable-next-line no-var-requires
const { Game } = require('../index');

const noop = () => undefined;

describe('Game component', () => {
  it('subscribes loop from context', () => {
    const context = {
      loop: {
        subscribe: jest.fn(),
        unsubscribe: jest.fn(),
      },
    };
    shallow(<Game toggleSound={noop} />, { context });
    expect(context.loop.subscribe).toHaveBeenCalledTimes(1);
  });
});
