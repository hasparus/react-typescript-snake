import React from 'react';

import { TextButton } from '../controls';

import { Consumer } from './sounds/Context';

type MuteUnmuteButtonProps = {
  onClick: () => void;
  enabled: boolean;
};
const MuteUnmuteButton = ({
  onClick,
  enabled,
}: MuteUnmuteButtonProps) => (
  <TextButton onClick={onClick}>
    {enabled ? 'Mute (M)' : 'Unmute (M)'}
  </TextButton>
);

const SoundControls = () => (
  <Consumer>
    {({ volume, toggleSound }) => (
      <MuteUnmuteButton
        onClick={toggleSound}
        enabled={volume === 1}
      />
    )}
  </Consumer>
);

export default SoundControls;
