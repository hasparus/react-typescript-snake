import React from 'react';

import { TextButton } from '../controls';

export type PauseButtonProps = {
  gamePaused: boolean;
  togglePause: (() => void) | null;
};
const PauseButton = ({
  gamePaused,
  togglePause,
}: PauseButtonProps) => {
  const disabled = !togglePause;

  return disabled ? (
    <TextButton disabled={disabled}>Pause (P)</TextButton>
  ) : (
    <TextButton onClick={togglePause as () => void}>
      {gamePaused ? 'Unpause' : 'Pause'} (P)
    </TextButton>
  );
};

export default PauseButton;
