import { partition, range } from 'ramda';

import { CELLS, collides, randomPosition, Vector2 } from '../core';
import { playSound, Sound } from '../sounds';

export type Candy = Vector2 & { id: number };

const CANDY_PROBABILITY = 1 / 8;
const MAX_CANDIES = 50;
let candyCounter = 0;

export function makeInitialCandies(count: number): Candy[] {
  candyCounter += count;
  return range(0, count).map(id => {
    const pos = Math.floor((Math.random() + id) * CELLS / count);
    return {
      id,
      x: pos,
      y: pos,
    };
  });
}

export function updateCandies(
  oldCandies: Candy[],
  candyDestroyerPosition: Vector2,
  forbiddenCells: Vector2[]
) {
  const [destroyedCandies, candies] = partition(
    collides(candyDestroyerPosition),
    oldCandies
  );

  const candiesEaten = oldCandies.length - candies.length;
  if (candiesEaten) {
    playSound(Sound.Beep);
  }

  if (
    Math.random() <
    CANDY_PROBABILITY -
      candies.length / (MAX_CANDIES / CANDY_PROBABILITY)
  ) {
    const newCandy = {
      id: candyCounter,
      ...randomPosition(),
    };
    const newCandyCollides = collides(newCandy);
    if (
      !newCandyCollides(candyDestroyerPosition) &&
      !candies.some(newCandyCollides) &&
      !forbiddenCells.some(newCandyCollides)
    ) {
      candies.push(newCandy);
    }
    candyCounter += 1;
  }

  return {
    candies,
    candiesEaten,
    destroyedCandies,
  };
}
