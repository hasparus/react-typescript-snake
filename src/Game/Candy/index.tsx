import React from 'react';
import styled from 'styled-components';

import { Color } from '../../color';
import { CELL_SIZE, ENTITY_SIZE } from '../core';

import { Candy as CandyT } from './candy';

const CANDY_MARGIN = ENTITY_SIZE / 5;

const CandyRect = styled.rect`
  stroke: ${Color.White};
  fill: transparent;
  stroke-width: ${ENTITY_SIZE / 10};
  position: relative;
  transform: rotate(45deg)
    translate3d(${CANDY_MARGIN}px, ${CANDY_MARGIN}px, 0);
  transform-origin: ${ENTITY_SIZE / 2}px ${ENTITY_SIZE / 2}px;
`;

export type CandyProps = CandyT;
const Candy = ({ x, y }: CandyProps) => (
  <svg x={x * CELL_SIZE} y={y * CELL_SIZE}>
    <CandyRect
      x={0}
      y={0}
      width={ENTITY_SIZE - CANDY_MARGIN * 2}
      height={ENTITY_SIZE - CANDY_MARGIN * 2}
    />
  </svg>
);

export default Candy;
export * from './candy';
