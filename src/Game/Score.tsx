import React from 'react';

export type ScoreProps = {
  score: number;
};

export const Score = ({ score }: ScoreProps) => (
  <div>Score: {score}</div>
);

export default Score;
