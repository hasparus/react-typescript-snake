import React from 'react';

import { TextButton } from '../controls';

export type LeaderboardButtonProps = {
  toggleLeaderboard: () => void;
};
const LeaderboardButton = ({
  toggleLeaderboard,
}: LeaderboardButtonProps) => (
  <TextButton onClick={toggleLeaderboard}>Leaderboard (L)</TextButton>
);

export default LeaderboardButton;
