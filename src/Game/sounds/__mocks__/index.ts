export const audioContext = jest.fn();
export const userControlledVolumeGain = jest.fn();
export const DEFAULT_VOLUME = 0;
export const playSound = jest.fn();
export const setVolume = jest.fn();
