import React, { createContext } from 'react';

import { audioContext, DEFAULT_VOLUME, setVolume } from './index';

const { Consumer, Provider: BaseProvider } = createContext<
  SoundContextType
>({
  volume: DEFAULT_VOLUME,
  toggleSound: () => undefined,
});

export type SoundContextType = {
  volume: number;
  toggleSound: () => void;
};

class Provider extends React.Component<{}, SoundContextType> {
  state = {
    volume: DEFAULT_VOLUME,
    toggleSound: () => {
      if (audioContext.state !== 'running') {
        audioContext.resume();
      }
      const volume = Number(!this.state.volume);
      this.setState({
        volume,
      });
      setVolume(volume);
    },
  };

  render() {
    return (
      <BaseProvider value={this.state}>
        {this.props.children}
      </BaseProvider>
    );
  }
}

export { Consumer, Provider };
