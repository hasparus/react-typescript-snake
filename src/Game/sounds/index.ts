export const audioContext = new AudioContext();
const gain = audioContext.createGain();
gain.gain.value = 0.1;

export const userControlledVolumeGain = audioContext.createGain();
export const DEFAULT_VOLUME = 0;
userControlledVolumeGain.gain.value = DEFAULT_VOLUME;

gain
  .connect(userControlledVolumeGain)
  .connect(audioContext.destination);

import Beep from './Beep8.wav';
import Explosion from './Explosion4.wav';

const bufferList: AudioBuffer[] = [];

export const enum Sound {
  Beep,
  Explosion,
}
const volumes: number[] = [0.1, 0.2];

[Beep, Explosion].map((path, index) =>
  fetch(path, {})
    .then(res => res.arrayBuffer())
    .then(arrayBuffer => {
      audioContext.decodeAudioData(arrayBuffer, buffer => {
        bufferList[index] = buffer;
        const s = audioContext.createBufferSource();
        s.buffer = buffer;
        s.connect(gain);
      });
    })
);

export function playSound(sound: Sound) {
  const source = audioContext.createBufferSource();
  source.buffer = bufferList[sound];
  gain.gain.value = volumes[sound];
  source.connect(gain);
  source.start();
}

export function setVolume(value: number) {
  userControlledVolumeGain.gain.value = value;
}

if (module.hot) {
  module.hot.dispose(() => {
    audioContext.close();
  });
}
