type Score = {
  player: string;
  value: number;
  created_at: number;
};

const API_URL = 'https://snake-api.glitch.me/v1';

const SCORES_URL = `${API_URL}/scores`;

export const sendScore = (score: Score) =>
  fetch(SCORES_URL, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(score),
    mode: 'cors',
  });

export const getScores = () => fetch(SCORES_URL);

export default Score;
